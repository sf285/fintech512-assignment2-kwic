# PSP for KWIC

## Problem description

Given a list of titles and a list of “words to ignore”, you are to write a program that generates a KWIC (Key Word In Context) index of the titles. In a KWIC-index, a title is listed once for each keyword that occurs in the title. The KWIC-index is alphabetized by keyword. Any word that is not one of the “words to ignore” is a potential keyword.

## Conceptual design


|                 Input                  | Lines of code |
|:--------------------------------------:|:-------------:|
| succeffully get information from input |      20       |
|     get splited line with keyword      |      15       |


|                  Sort                  | Lines of code |
|:--------------------------------------:|:-------------:|
| Give each Captal keywords to each line |      20       |
|   finish to consturct whole order      |      20       |


|                   Main                   | Lines of code |
|:----------------------------------------:|:-------------:|
| use class/functions to give right answer |      20       |
|                                          |               |



## Planning

|                    Total                    | 180 mins |
|:-------------------------------------------:|:--------:|
|   succeffully get information from input    |    60    |
|        get splited line with keyword        |    15    |
|   Give each Captal keywords to each line    |    45    |
|       finish to consturct whole order       |    30    |
|          all done for consturction          |    15    |
|  use class/functions to give right answer   |    15    |


## Development

|                      Issues                       |                                                                                                       |
|:-------------------------------------------------:|
| Spent too many time to design the whole struture  |
|                                                   |   
|                                                   |                                                                                        |

|              Development Time               | Total: 225 mins |
|:-------------------------------------------:|-----------------|
|   succeffully get information from input    | 80              |
|        get splited line with keyword        | 30              |
|  Give each Captal keywords to each line     | 40              |
|       finish to consturct whole order       | 60              |
|          all done for consturction          | 15              |
|  use class/functions to give right answer   | 15              |


|                 Input                  | Lines of code |
|:--------------------------------------:|:-------------:|
| succeffully get information from input |      24       |
|     get splited line with keyword      |      16       |

|                  Sort                  | Lines of code |
|:--------------------------------------:|:-------------:|
| Give each Captal keywords to each line |      30       |
|    finish to consturct whole order     |      10       |
|       all done for consturction        |      10       |

|                   Main                    | Lines of code |
|:-----------------------------------------:|:-------------:|
| call class/functions to give right answer |      10       |


## Testing

|            Defect             | Testing Time |
|:-----------------------------:|--------------|
|    IgnoreTest()  No Defect    | 10 mins      |
|   KeyWordsTest() No defect    | 20 mins      |
|    KeyLineTest() No Defect    | 10 mins      |
|            CKeyL()            | 5 min        |
|            CKey()             | 5 min        |
|            outL()             | 5 min        |

## Evaluation

- Total time: 300 mins
- Notable issues:
    - use arraylist and arraylist of arraylist to store information
    - sort the order for each line








