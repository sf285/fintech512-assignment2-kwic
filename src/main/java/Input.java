import java.util.*;
import java.io.*;

public class  Input {
    private ArrayList<String> ignore_words;
    private ArrayList<String> key_lines;
    private ArrayList<ArrayList<String>> key_words;

    public Input(){
        this.ignore_words=new ArrayList<String>();
        this.key_lines=new ArrayList<String>();
        this.key_words=new ArrayList<ArrayList<String>>();
    }
    public void split_line() {

        BufferedReader lines= new BufferedReader(new InputStreamReader(System.in));
        String cur_word;
        int check=0;
        try {
            while ((cur_word = lines.readLine()) != null){
                if (cur_word.equals("::")){
                    check = 1;
                    continue;
                }
                if (check == 0){
                    ignore_words.add(cur_word);
                }
                else {
                    key_lines.add(cur_word+'\n');
                    String[] strS = cur_word.split(" ");
                    ArrayList<String> strL= new ArrayList<String>(Arrays.asList(strS));
                    key_words.add(strL);
                }
            }
        }catch (IOException ioe) {
            System.out.println(ioe);
        }
    }


    public ArrayList<String> getIgnore_words() {
        return ignore_words;
    }
    public ArrayList<String> getKey_lines() {
        return key_lines;
    }
    public ArrayList<ArrayList<String>> getKey_Words() {
        return key_words;
    }

}
