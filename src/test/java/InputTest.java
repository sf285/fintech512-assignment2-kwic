import static org.junit.jupiter.api.Assertions.*;
import java.io.*;
import java.util.*;
import org.junit.jupiter.api.Test;

class InputTest {
    @Test
    void KeyWordsTest(){
        Input input = new Input();
        String str = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n";
        ByteArrayInputStream strIn = new ByteArrayInputStream(str.getBytes());
        System.setIn(strIn);
        input.split_line();

        //assertEquals(input.getIgnores(),2);
        ArrayList<String> inputStr1 = new ArrayList<String>();
        ArrayList<String> inputStr2 = new ArrayList<String>();
        ArrayList<ArrayList<String>> inputStr = new ArrayList<ArrayList<String>>();
        inputStr1.add("Descent"); inputStr1.add("of"); inputStr1.add("Man");/* inputStr1.add("\n");*/
        inputStr2.add("The"); inputStr2.add("Ascent"); inputStr2.add("of");inputStr2.add("Man");/*inputStr2.add("\n");*/
        inputStr.add(inputStr1);
        inputStr.add(inputStr2);

        assertEquals(input.getKey_Words(),inputStr);

    }
    @Test
    void KeyLineTest(){
        Input input = new Input();
        String str = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        ByteArrayInputStream strIn = new ByteArrayInputStream(str.getBytes());
        System.setIn(strIn);
        input.split_line();

        //assertEquals(input.getIgnores(),2);
        ArrayList<String> inputStr = new ArrayList<String>();
        inputStr.add("Descent of Man" + "\n");
        inputStr.add("The Ascent of Man"+ "\n");
        inputStr.add("The Old Man and The Sea"+ "\n");
        inputStr.add( "A Portrait of The Artist As a Young Man"+ "\n");
        inputStr.add("A Man is a Man but Bubblesort IS A DOG"+"\n");

        assertEquals(input.getKey_lines(),inputStr);

    }

    @Test
    void IgnoreTest(){
        Input input= new Input();
        String str_Ignore = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        ByteArrayInputStream strIn = new ByteArrayInputStream(str_Ignore.getBytes());
        System.setIn(strIn);
        input.split_line();

        ArrayList<String> AIgnore = new ArrayList<String>();
        AIgnore.add("is");
        AIgnore.add("the");
        AIgnore.add("of");
        AIgnore.add("and");
        AIgnore.add("as");
        AIgnore.add("a");
        AIgnore.add("but");

        assertEquals(AIgnore,input.getIgnore_words());
    }

}