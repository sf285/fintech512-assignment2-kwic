import static org.junit.jupiter.api.Assertions.*;
import java.io.*;
import java.util.*;
import org.junit.jupiter.api.Test;

class Sort_ConTest {
    @Test
    void CKeyL(){
        Input input = new Input();
        Sort_Con ak= new Sort_Con();
        String str = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n";
        ByteArrayInputStream strIn = new ByteArrayInputStream(str.getBytes());
        System.setIn(strIn);
        input.split_line();
        ak.K_line(input);



        //assertEquals(input.getIgnores(),2);
        ArrayList<String> inputStr1 = new ArrayList<String>();
        ArrayList<String> inputStr2 = new ArrayList<String>();
        ArrayList<String> inputStr3 = new ArrayList<String>();
        ArrayList<String> inputStr4 = new ArrayList<String>();
        ArrayList<ArrayList<String>> inputStr = new ArrayList<ArrayList<String>>();
        inputStr1.add("DESCENT");
        inputStr1.add("DESCENT of man");
        inputStr2.add("MAN");
        inputStr2.add("descent of MAN");
        inputStr3.add("ASCENT");
        inputStr3.add("the ASCENT of man");
        inputStr4.add("MAN");
        inputStr4.add("the ascent of MAN");

        inputStr.add(inputStr1);
        inputStr.add(inputStr2);
        inputStr.add(inputStr3);
        inputStr.add(inputStr4);

        assertEquals(ak.getKL(),inputStr);

    }

    @Test
    void CKey(){
        Input input = new Input();
        Sort_Con ak= new Sort_Con();
        String str = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n"+
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n";
        ByteArrayInputStream strIn = new ByteArrayInputStream(str.getBytes());
        System.setIn(strIn);
        input.split_line();
        ak.K_line(input);



        //assertEquals(input.getIgnores(),2);
        ArrayList<String> inputStr1 = new ArrayList<String>();
        inputStr1.add("ARTIST");
        inputStr1.add("ASCENT");
        inputStr1.add("DESCENT");
        inputStr1.add("MAN");
        inputStr1.add("MAN");
        inputStr1.add("MAN");
        inputStr1.add("MAN");
        inputStr1.add("OLD");
        inputStr1.add("PORTRAIT");
        inputStr1.add("SEA");
        inputStr1.add("YOUNG");

        assertEquals(ak.getK(),inputStr1);
    }
    @Test
    void outL() {
        Input input = new Input();
        Sort_Con ak= new Sort_Con();
        String str = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        ByteArrayInputStream strIn = new ByteArrayInputStream(str.getBytes());
        System.setIn(strIn);
        input.split_line();
        ak.K_line(input);
        ak.ordL();
        ArrayList<String> inputStr = new ArrayList<String>();
        inputStr.add("a portrait of the ARTIST as a young man");
        inputStr.add("the ASCENT of man");
        inputStr.add("a man is a man but BUBBLESORT is a dog");
        inputStr.add("DESCENT of man");
        inputStr.add("a man is a man but bubblesort is a DOG");
        inputStr.add("descent of MAN");
        inputStr.add("the ascent of MAN");
        inputStr.add("the old MAN and the sea");
        inputStr.add("a portrait of the artist as a young MAN");
        inputStr.add("a MAN is a man but bubblesort is a dog");
        inputStr.add("a man is a MAN but bubblesort is a dog");
        inputStr.add("the OLD man and the sea");
        inputStr.add("a PORTRAIT of the artist as a young man");
        inputStr.add("the old man and the SEA");
        inputStr.add("a portrait of the artist as a YOUNG man");
        assertEquals(ak.getF_keyL(),inputStr);
    }

}